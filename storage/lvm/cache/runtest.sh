#!/bin/bash
#
# Copyright (c) 2021 Red Hat, Inc. All rights reserved.
#
# This copyrighted material is made available to anyone wishing
# to use, modify, copy, or redistribute it subject to the terms
# and conditions of the GNU General Public License version 2.
#
# This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#

source ../../include/libstqe.sh

function startup
{
    stqe_init_fwroot "master"
}

function cleanup
{
    stqe_fini_fwroot
}

TEST_CASE_PATH="lvm/cache/cache_basic.py"
function runtest
{
    cki_cd $(stqe_get_fwroot)
    cki_run_cmd_pos "stqe-test run -t $TEST_CASE_PATH"
    typeset -i rc=$?
    cki_pd
    (( rc != 0 )) && return $CKI_FAIL || return $CKI_PASS
}

cki_debug
cki_main
exit $?
